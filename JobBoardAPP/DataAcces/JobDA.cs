﻿using JobBoardAPP.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JobBoardAPP.DataAcces
{
    public class JobDA
    {
        public List<Job> getAllJobs()
        {
            using (var db = new JobContext())
            {
                return db.Jobs.ToList();
            }
        }

        public void AddJob(Job job)
        {
            using (var db = new JobContext())
            {

                try
                {
                    db.Jobs.Add(job);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                    throw;
                }





            }
        }

        public Job findAJob(int idJob)
        {
            using (var db = new JobContext())
            {
                try
                {

                    return db.Jobs.Find(idJob);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }


        public void UpdateJob(Job job)
        {
            using (var db = new JobContext())
            {
                try
                {

                    db.Jobs.Attach(job);
                    db.Entry(job).State = EntityState.Modified;
                    db.SaveChanges();

                }
                catch (Exception ex)
                {

                    throw;
                }
            }

        }

        public bool DeleteJob(Job job)
        {
            bool result;
            using (var db = new JobContext())
            {
                try
                {
                                       
                    db.Entry(job).State = EntityState.Deleted;
                    db.SaveChanges();
                    result = true;

                }
                catch (Exception ex)
                {
                    result = false;
                    throw;
                }


                return result;
            
            }
        }



    }
}