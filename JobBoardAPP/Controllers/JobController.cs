﻿using JobBoardAPP.DataAcces;
using JobBoardAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JobBoardAPP.Controllers
{
    public class JobController : Controller
    {
        private JobDA _db;

        public JobController()
        {
            _db = new JobDA();
        }


        // GET: Job
        public ActionResult Index()
        {
            var model = _db.getAllJobs();

            return View(model);
        }

        // GET: Job/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Job/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Job/Create
        [HttpPost]
        public ActionResult Create(Job job)
        {

            string sdate1 = Request["txt_dtpicker1"];
            string sdate2 = Request["txt_dtpicker2"];

            if (string.IsNullOrEmpty(sdate1)||string.IsNullOrEmpty(sdate2))
            {
                return View();
            }
            else
            {
                DateTime date1 = Convert.ToDateTime(sdate1);
                DateTime date2 = Convert.ToDateTime(sdate2);

                job.createdAt = date1;
                job.expiresAt = date2;

            }


            try
            {
                if (ModelState.IsValid)
                {
                    _db.AddJob(job);
                }

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: Job/Edit/5
        public ActionResult Edit(int id)
        {
            var model=  _db.findAJob(id);

            ViewBag.createDate = model.createdAt.ToString("dd/MM/yyyy");
            ViewBag.expireDate = model.expiresAt.ToString("dd/MM/yyyy");



            return View(model);
        }

        // POST: Job/Edit/5
        [HttpPost]
        public ActionResult Edit(Job job)
        {
            try
            {
                DateTime date1 = Convert.ToDateTime(Request["txt_dtpicker1"]);
                DateTime date2 = Convert.ToDateTime(Request["txt_dtpicker2"]);

                job.createdAt = date1;
                job.expiresAt = date2;

              


                _db.UpdateJob(job);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Job/Delete/5
        public ActionResult Delete(int id)
        {
            var model = _db.findAJob(id);
            ViewBag.createDate = model.createdAt.ToString("dd/MM/yyyy");
            ViewBag.expireDate = model.expiresAt.ToString("dd/MM/yyyy");
            return View(model);
        }

        // POST: Job/Delete/5
        [HttpPost]
        public ActionResult Delete(Job job)
        {
            try
            {
                bool resul = _db.DeleteJob(job);

                if (resul)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

               
            }
            catch
            {
                return View();
            }
        }
    }
}
