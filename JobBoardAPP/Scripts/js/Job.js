﻿(function (w, d, $) {
    w.main = {
        init: function () {
            setEvents();
        },

    };

    function setEvents() {

  
        //set datetimepicker format 
        $('#datetimepicker1').datetimepicker({
            format: 'dd/mm/yyyy',
            minView: 'month'
        });
        $('#datetimepicker2').datetimepicker({
            format: 'dd/mm/yyyy',
            minView: 'month'
        });


    };//fin setEvents

   

    $(d).ready(main.init);
})(window, document, jQuery);