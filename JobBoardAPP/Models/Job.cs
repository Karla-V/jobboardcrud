﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JobBoardAPP.Models
{
    public class Job
    {
        [Key]   
        [Display(Name ="Job")]
        public int jobb { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name ="Job Title")]
        public string jobTitle { get; set; }
                        
        [StringLength(300)]
        [Display(Name = "Description")]
        public string descripcion { get; set; }
        
        [DataType(DataType.Date)]  
        [Display(Name ="Created At")]
        public DateTime createdAt { get; set; }
        
        [DataType(DataType.Date)]      
        [Display(Name ="Expires At")]
        public DateTime expiresAt { get; set; }

    }
}