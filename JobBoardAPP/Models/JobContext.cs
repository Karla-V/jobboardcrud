﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JobBoardAPP.Models
{
    public class JobContext: DbContext
    {
        public JobContext()
            :base("JobConnection")
        {
                
        }


        public DbSet<Job> Jobs { get; set; }

    }
}